# Kamran's (public) SSH keys:

If you need my help, you will need to put my SSH keys in `.ssh/authorized_keys` file on your server, inside the home directory of the user of your choice. I would need admin/root access to the server, this means, you either place thse keys under `/root/.ssh/authorized_keys` , or in  `.ssh/authorized_keys` file of a user with `sudo` privileges.


The text below contains (public part of) two of my SSH keys. One is RSA format and the other is ED25519 format. These keys are also available as separate files in this repository.

```
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAx+5TIvYxBryI9d3DjvAeDv4q8xycNbXAAmfOIwhXL0D7So67MpmnQavwHaE/dVsGzP/9XMcidOYl7xBK0aN0fozApThWHaeKpWuJC2w4qE0ijD6tCAbnA7/Wach1rEmGVtRKo5B5lpPXuTedoixM/St/T46wnLFIwsDdFOTMyk9QHRtQ+uJAKv/lkuimMZjDRWeJE5ggwR4SNsc306R9ArnDBdj9HJ3xeUb5rqiBCe1qV3a5k8MpjsaIgG8KPx5dvXRhOTFE4ueh+2wLMy6ydy68NU5kltBtxqBA8CYbEyYmUL/cqRdx6ZVkL8AT5Pv44e2JRnN3kE70HJADfoDX5w== kamranazeem@gmail.com


ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDROn5fovT1lZnJWPu3/cWuSjUyxh5pjme3JMvuWcdZg kamranazeem@gmail.com
```

If you are new to this, follow the step below:

```
(Switch to root)
$ sudo -i

(Run the following commands as root)

# mkdir /root/.ssh

# chmod 0700 /root/.ssh

# echo 'ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAx+5TIvYxBryI9d3DjvAeDv4q8xycNbXAAmfOIwhXL0D7So67MpmnQavwHaE/dVsGzP/9XMcidOYl7xBK0aN0fozApThWHaeKpWuJC2w4qE0ijD6tCAbnA7/Wach1rEmGVtRKo5B5lpPXuTedoixM/St/T46wnLFIwsDdFOTMyk9QHRtQ+uJAKv/lkuimMZjDRWeJE5ggwR4SNsc306R9ArnDBdj9HJ3xeUb5rqiBCe1qV3a5k8MpjsaIgG8KPx5dvXRhOTFE4ueh+2wLMy6ydy68NU5kltBtxqBA8CYbEyYmUL/cqRdx6ZVkL8AT5Pv44e2JRnN3kE70HJADfoDX5w== kamranazeem@gmail.com' >> /root/.ssh/authorized_keys


# echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDROn5fovT1lZnJWPu3/cWuSjUyxh5pjme3JMvuWcdZg kamranazeem@gmail.com' >> /root/.ssh/authorized_keys
```

